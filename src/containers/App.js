import React, { Component } from "react";
import { connect } from "react-redux";
import { connectKad } from "../module/Api";
import { onTransactionEvent } from "../module/Blockchain";
import BtnMining from "../components/BtnMining";
import TextToken from "../components/TextToken";
import TextUserId from "../components/TextUserId";
import FormTransaction from "../components/FormTransaction";
import { AppBar, Toolbar, IconButton, Typography } from "@material-ui/core";
import { Menu } from "@material-ui/icons";

class App extends Component {
  render() {
    const { api, dispatch, blockchain } = this.props;
    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <IconButton color="inherit" aria-label="Menu">
              <Menu />
            </IconButton>
            <Typography variant="title" color="inherit">
              Blockchain
            </Typography>
          </Toolbar>
        </AppBar>
        <font size="5">
          <TextUserId api={api} />
        </font>
        <br />
        <font size="5">
          <TextToken blockchain={blockchain} />
        </font>
        <BtnMining api={api} dispatch={dispatch} />
        <FormTransaction api={api} dispatch={dispatch} />
      </div>
    );
  }

  componentWillMount() {
    const { dispatch } = this.props;
    const data = connectKad(dispatch);
    onTransactionEvent(dispatch,data. connection,data.blockchain);
  }
}

const mapStateToProps = state => {
  return {
    api: state.api,
    blockchain: state.blockchain
  };
};

export default connect(mapStateToProps)(App);
