import webrtc from "../lib/webrtc";
import sha1 from "sha1";
import Function from "./func";
import Events from "events";

const def = {
  STORE: "STORE",
  FINDNODE: "FINDNODE",
  FINDNODE_R: "FINDNODE_R",
  FINDVALUE: "FINDVALUE",
  FINDVALUE_R: "FINDVALUE_R",
  PING: "PING",
  PONG: "PONG",
  ONCOMMAND: "ONCOMMAND",
  ADD_KNODE: "ADD_KNODE",
  BLOADCAST: "BLOADCAST"
};

//networkLayer
function packetFormat(network, data) {
  return JSON.stringify({
    layer: "networkLayer",
    network: network,
    kid: nodeId,
    data: data,
    hash: sha1(Date.now() + JSON.stringify(data))
  });
}

let f, nodeId;

export default class Kademlia {
  constructor(_kid) {
    this.k = 20;
    this.kid = _kid;
    nodeId = _kid;
    this.dataList = [];
    this.keyValueList = [];
    this.ref = {};
    this.ev = new Events.EventEmitter();
    this.pingResult = [];

    this.kbuckets = new Array(160);
    for (let i = 0; i < 160; i++) {
      let kbucket = [];
      this.kbuckets[i] = kbucket;
    }

    f = new Function(this.k, this.kbuckets);
  }

  async ping(peer) {
    console.log("ping");

    const sendData = { target: peer.kid };
    peer.send(packetFormat(def.PING, sendData));

    this.pingResult[peer.kid] = false;

    await setTimeout(() => {
      if (this.pingResult[peer.kid]) {
        console.log("ping success");
        return true;
      } else {
        console.log("ping fail", peer.kid);
        return false;
      }
    }, 3 * 1000);
  }

  store(_sender, _key, _value) {
    const peer = f.getCloseEstPeer(_key);

    console.log(def.STORE, "next", peer.kid, "target", _key);

    const sendData = {
      sender: _sender,
      key: _key,
      value: _value
    };

    const result = this.ping(peer);

    if (result) {
      peer.send(packetFormat(def.STORE, sendData));
      console.log("store done");
    } else {
      console.log("store faile");
    }
  }

  findNode(_targetID, peer) {
    console.log("findnode", _targetID);

    const sendData = { targetKey: _targetID };
    peer.send(packetFormat(def.FINDNODE, sendData));
  }

  findValue(key) {
    return new Promise(resolve => {
      const peer = f.getCloseEstPeer(key);
      const result = this.ping(peer);
      if (result) {
        const sendData = { targetKey: key };
        peer.send(packetFormat(def.FINDNODE, sendData));
      }
      this.ev.on(def.FINDVALUE, data => {
        console.log("findValue success");
        this.keyValueList[key] = data;
        resolve(data);
      });

      setTimeout(() => {
        console.log("findValue fail");
        resolve(false);
      }, 10 * 1000);
    });
  }

  addknode(peer) {
    let exist = false;
    f.getAllPeers().some(v => {
      if (v.kid === peer.kid) {
        console.log("addknode", "exist so failed");
        exist = true;
        return 0;
      }
    });

    if (!exist) {
      const num = f.distance(this.kid, peer.kid);
      const kbucket = this.kbuckets[num];

      kbucket.push(peer);
      console.log("addknode kbuckets", "peer.kid:", peer.kid);
      this.ev.emit(def.ADD_KNODE);
    }
  }

  bloadcast(data) {
    this.onBloadcast(packetFormat(def.BLOADCAST, data.toString()));
  }

  onRequest(str) {
    const packet = JSON.parse(str);

    switch (packet.network) {
      case def.STORE:
        this.onStore(packet);
        break;
      case def.FINDNODE:
        this.onFindNode(packet);
        break;
      case def.FINDNODE_R:
        this.onFindNodeReturn(packet);
        break;
      case def.FINDVALUE:
        this.onFindValue(packet);
        break;
      case def.FINDVALUE_R:
        this.onFindValueReturn(packet);
        break;
      case def.PING:
        this.onPing(packet);
        break;
      case def.PONG:
        this.onPong(packet);
        break;
      case def.BLOADCAST:
        this.onBloadcast(str);
        break;
    }
    this.maintain(packet);
  }

  onStore(packet) {
    console.log("on store", packet.kid);

    const data = packet.data;

    const mine = f.distance(this.kid, data.key);
    const close = f.getCloseEstDist(data.key);
    if (mine > close) {
      console.log("store transfer", "\ndata", data);
      this.store(data.sender, data.key, data.value);
    } else {
      console.log("store arrived", "\ndata", data);
    }
    this.keyValueList[data.key] = data.value;

    const target = data.sender;
    if (data.key === this.kid && !f.kidExist(target)) {
      if (data.value.includes("sdp")) {
        if (data.value.includes("offer")) {
          console.log("kad received offer", data.sender);

          this.ref.target = new webrtc("answer");

          async function sync(self) {
            const result = await self.answer(target, data.value, self.ref);
            if (result) console.log("findnode answer seccess", target);
            else console.log("findnode answer fail", target);
          }
          sync(this);
        } else if (data.value.includes("answer")) {
          console.log("kad received answer", data.sender);

          this.ref.target.rtc.signal(JSON.parse(data.value));
        }
      }
    }
  }

  onFindNode(packet) {
    console.log("on findnode", packet.kid);
    const data = packet.data;
    const sendData = { closeIDs: f.getCloseIDsAsJson(data.targetKey) };
    if (f.getPeerFromKid(packet.kid) != null) {
      f.getPeerFromKid(packet.kid).send(packetFormat(def.FINDNODE_R, sendData));
    }
  }

  onFindNodeReturn(packet) {
    const data = packet.data;

    const ids = data.closeIDs;
    console.log("on findnode-r", ids);

    async function sync(self) {
      for (let target of ids) {
        console.log("findnode offer", target);
        self.ref.target = new webrtc("offer");
        const result = await self.offer(target, self.ref);
        if (result) console.log("findnode offer seccess", target);
        else console.log("findnode offer fail", target);
      }
    }
    sync(this);
  }

  onFindValue(packet) {
    console.log("on findvalue", packet.kid);

    const data = packet.data;

    function send(self) {
      const sendData = { targetValue: self.keyValueList[data.targetKey] };
      f.getPeerFromKid(packet.kid).send(
        packetFormat(def.FINDVALUE_R, sendData)
      );
    }

    if (this.keyValueList.toString().includes(data.targetKey)) {
      send(this);
    } else {
      async function sync(self) {
        const result = await self.findValue(data.targetKey);
        if (result !== false) {
          send(this);
        }
      }
      sync(this);
    }
  }

  onFindValueReturn(packet) {
    const data = packet.data;
    this.ev.emit(def.FINDVALUE, data);
  }

  onPing(packet) {
    const data = packet.data;

    if (data.target === this.kid) {
      console.log("ping received");
      f.getAllPeers().some(v => {
        if (v.kid === packet.kid) {
          const sendData = { target: packet.kid };
          v.send(packetFormat(def.PONG, sendData));
        }
      });
    }
  }

  onPong(packet) {
    const data = packet.data;

    if (data.target === this.kid) {
      console.log("pong received", packet.kid);
      this.pingResult[packet.kid] = true;
    }
  }

  onBloadcast(str) {
    f.getAllPeers().some(v => {
      const result = this.ping(v);
      if (result) {
        //接続範囲を広げる
        this.findNode(this.kid, v);
        v.send(str);
        console.log("bloadcast done");
      } else {
        console.log("bloadcast faile");
      }
    });
  }

  cleanDiscon() {
    for (let i = 0; i < 160; i++) {
      this.kbuckets[i].some((v, j) => {
        if (!v.isConnected) {
          this.kbuckets[i].splice(j, 1);
          console.log("kbuckets cleaned");
          return 0;
        }
      });
    }
  }

  maintain(packet) {
    console.log("mainatin");

    this.cleanDiscon();

    if (packet.kid !== this.kid && f.getPeerNum() > 0) {
      const inx = f.distance(this.kid, packet.kid);
      const kbucket = this.kbuckets[inx];
      if (f.kidExist(packet.kid)) {
        console.log("@maintain", "kid exist kbucket", inx);
        kbucket.some((v, j) => {
          if (v.kid === packet.kid) {
            console.log("@maintain", "Moves it to the tail of the list");
            let peer = v;
            kbucket.splice(j, 1);
            this.addknode(peer);
            return 0;
          }
        });
      } else {
        if (kbucket.length < this.k) {
          console.log(
            "@maintain",
            "bucket has fewer than k entries",
            f.getPeerNum()
          );

          const target = packet.kid;
          this.ref.target = new webrtc("offer");
          this.offer(target, this.ref);
        } else {
          console.log("@maintain", "bucket has mutch more than k entries");
          this.ping(kbucket[0]).then(success => {
            const peer = kbucket[0];
            kbucket.splice(0, 1);
            if (success) {
              console.log(
                "@maintain",
                "it is moved to the tail of the list, and the new sender’s contact is discarded."
              );
              this.addknode(peer);
            } else {
              console.log(
                "@maintain",
                "it is evicted from the k­bucket and the new sender inserted at the tail."
              );

              const target = packet.kid;
              this.ref.target = new webrtc("offer");
              this.offer(target, this.ref);
            }
          });
        }
      }
    }

    console.log("maintain done");    
  }

  offer(target, r) {
    return new Promise(resolve => {
      console.log("kad offer", target);
      r.target.connecting(target);

      r.target.rtc.on("error", err => {
        console.log("kad offer connect error", target, err);

        resolve(false);
      });

      r.target.rtc.on("signal", sdp => {
        console.log("kad offer store", target);

        if (f.getCloseEstPeer(target) !== target)
          this.store(this.kid, target, JSON.stringify(sdp));
      });

      r.target.rtc.on("connect", () => {
        console.log("kad offer connected", target);
        console.log(this.kbuckets);
        r.target.connected();
        this.addknode(r.target);

        resolve(true);
      });

      r.target.rtc.on("data", data => {
        console.log("kad rtc recieved", target, data.toString());
        this.onCommand(data);
      });

      setTimeout(() => {
        resolve(false);
      }, 3 * 1000);
    });
  }

  answer(target, sdpStr, r) {
    return new Promise(resolve => {
      r.target.connecting(target);
      console.log("kad answer", target);

      r.target.rtc.on("error", err => {
        console.log("error", target, err);
        resolve(false);
      });

      r.target.rtc.signal(JSON.parse(sdpStr));

      r.target.rtc.on("signal", sdp => {
        this.store(this.kid, target, JSON.stringify(sdp));
      });

      r.target.rtc.on("connect", () => {
        console.log("kad answer connected", target);
        console.log(this.kbuckets);
        r.target.connected();
        this.addknode(r.target);

        resolve(true);
      });

      r.target.rtc.on("data", data => {
        console.log("kad rtc recieved", target, data.toString());
        this.onCommand(data);
      });

      setTimeout(() => {
        resolve(false);
      }, 3 * 1000);
    });
  }

  onCommand(dataLinkLayer) {
    const networkLayer = JSON.parse(dataLinkLayer);
    console.log("oncommand", networkLayer.hash);

    if (!this.dataList.toString().includes(networkLayer.hash)) {
      this.dataList.push(networkLayer.hash);

      this.onRequest(dataLinkLayer);

      this.ev.emit(def.ONCOMMAND, dataLinkLayer);
    }
  }
}
