import webrtc from "../lib/webrtc";
import Kademlia from "./kademlia";
import client from "socket.io-client";
import events from "events";

let peerOffer, peerAnswer;
let socket;

const def = {
  ONCOMMAND: "ONCOMMAND",
  ADD_KNODE: "ADD_KNODE",
  GET_OFFER: "GET_OFFER",
  ADD_ANSWER: "ADD_ANSWER",
  ADD_OFFER: "ADD_OFFER",
  OFFER: "OFFER"
};

export default class Connect {
  constructor(_userId) {
    this.userId = _userId;
    console.log("program", this.userId);

    this.kad = new Kademlia(this.userId);
    this.ev = new events.EventEmitter();

    this.kad.ev.on(def.ONCOMMAND, datalinkLayer => {
      if (datalinkLayer.includes("p2ch")) {
        const networkLayer = JSON.parse(datalinkLayer).data;
        this.ev.emit("p2ch", networkLayer);
      }
    });

    socket = client.connect("http://35.196.94.146:3002");

    socket.on("connect", () => {
      console.log("socket connected");
      peerOffer = new webrtc("offer");
      peerAnswer = new webrtc("answer");
      socket.emit(def.GET_OFFER, this.userId);
    });

    socket.on(def.GET_OFFER, json => {
      if (json == undefined || json.kid === this.userId) {
        this.offerFirst();
      } else {
        async function sync(self) {
          const result = await self.answerFirst(json);
          if (!result) {
            console.log("@cli", "first answer failed");
            peerAnswer.failed();
            socket.emit(def.GET_OFFER, self.userId);
          }
        }
        sync(this);
      }
    });

    socket.on(def.ADD_ANSWER, str => {
      const data = JSON.parse(str);
      if (!peerOffer.signaling && data.target === this.userId) {
        peerOffer.connecting(data.kid);
        peerOffer.rtc.signal(JSON.parse(data.sdp));
      }
    });
  }

  offerFirst() {
    console.log("@cli", "offer first");
    peerOffer = new webrtc("offer");

    peerOffer.rtc.on("signal", data => {
      const json = {
        type: def.OFFER,
        kid: this.userId,
        sdp: JSON.stringify(data)
      };

      socket.emit(def.ADD_OFFER, JSON.stringify(json));
    });

    peerOffer.rtc.on("connect", () => {
      console.log("@cli", "offer first connected", peerOffer.kid);

      peerOffer.connected();

      this.kad.addknode(peerOffer);
      this.kad.findNode(peerOffer.kid, peerOffer);

      this.offerFirst();
      //socket.disconnect();
    });

    peerOffer.rtc.on("data", data => {
      this.onCommand(data);
    });
  }

  answerFirst(data) {
    console.log("@cli", "answer first");

    return new Promise(resolve => {
      peerAnswer = new webrtc("answer");

      peerAnswer.connecting(data.kid);
      peerAnswer.rtc.signal(JSON.parse(data.sdp));

      setTimeout(() => {
        resolve(false); //デバッグ時用のタイムアウト
      }, 2 * 1000);

      peerAnswer.rtc.on("signal", v => {
        const json = {
          kid: this.userId,
          target: data.kid,
          sdp: JSON.stringify(v)
        };
        socket.emit(def.ADD_ANSWER, JSON.stringify(json));
      });

      peerAnswer.rtc.on("error", err => {
        console.log("error", err);

        resolve(false);
      });

      peerAnswer.rtc.on("connect", () => {
        console.log("@cli", "answer first connected", peerAnswer.kid);
        peerAnswer.connected();
        this.offerFirst();

        this.kad.addknode(peerAnswer);
        this.kad.findNode(peerAnswer.kid, peerAnswer);

        resolve(true);
      });

      peerAnswer.rtc.on("data", data => {
        this.onCommand(data);
      });
    });
  }

  onCommand(datalinkLayer) {
    this.kad.onCommand(datalinkLayer);
  }

  send(data) {
    this.kad.bloadcast(data);
  }
}
