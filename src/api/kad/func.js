import Util from '../lib/util'
const util = new Util()

export default class Function {
    constructor(_k, _kbuckets) {
        this.k = _k
        this.kbuckets = _kbuckets
    }

    distance(a16, b16) {
        let a = util.convertBase(a16, 16, 2).toString().split('')
        let b = util.convertBase(b16, 16, 2).toString().split('')

        let xor
        if (a.length > b.length) xor = new Array(a.length)
        else xor = new Array(b.length)

        for (let i = 0; i < xor.length; i++) {
            xor[i] = parseInt(a[i]) ^ parseInt(b[i])
        }
        let xored = xor.toString().replace(/,/g, '')
        let n10 = parseInt(util.convertBase(xored, 2, 10).toString())

        let n, i
        for (i = 0; ; i++) {
            n = 2 ** i
            if (n > n10) break
        }

        return i
    }

    getCloseEstPeer(_key) {
        let mini = 160
        let peer
        for (let i = 0; i < 160; i++) {
            this.kbuckets[i].some((v) => {
                if (this.distance(_key, v.kid) < mini) {
                    mini = this.distance(_key, v.kid)
                    peer = v
                }
            })
        }
        return peer
    }

    getAllPeers() {
        let peers = []
        for (let i = 0; i < 160; i++) {
            this.kbuckets[i].some((v) => {
                peers.push(v)
            })
        }
        return peers
    }

    getPeerNum() {
        let num = 0
        this.getAllPeers().some((v) => {
            num++
        })
        return num
    }

    getCloseIDsAsJson(targetID) { //Returns information for the k nodes it knows about closest to the target ID        
        let list = []
        const that = this
        this.getAllPeers().some((v) => {
            if (v.kid !== targetID) {
                if (list.length < that.k) list.push(v.kid)
                else {
                    for (let i = 0; i < list.length; i++) {
                        if (this.distance(list[i], targetID) > this.distance(v.kid, targetID)) {
                            list[i] = v.kid
                        }
                    }
                }
            }
        })

        return list
    }

    getCloseEstDist(_key) {
        let mini = 160
        const that = this
        for (let i = 0; i < 160; i++) {
            this.kbuckets[i].some((v) => {
                if (this.distance(_key, v.kid) < mini) {
                    mini = this.distance(_key, v.kid)
                }
            })
        }
        return mini
    }

    kidExist(id) {
        let exist = false
        this.getAllPeers().some((v) => {
            if (v.kid === id) {
                exist = true
                return 0
            }
        })
        return exist
    }

    getPeerFromKid(_kid) {
        let peer
        this.getAllPeers().some((v) => {
            if (v.kid === _kid) {
                peer = v
                return 0
            }
        })
        return peer
    }
}