const wrtc = require("wrtc");
const simplePeer = require("simple-peer");

export default class webrtc {
  constructor(_type) {
    this.rtc;
    this.kid;
    this.target;
    this.isConnected = false;
    this.isCheking = false;
    this.type = _type;
    switch (_type) {
      case "offer":
        console.log("webrtc", "offer");
        this.initOffer();
        break;
      case "answer":
        console.log("webrtc", "answer");
        this.initAnswer();
        break;
    }

    this.rtc.on("data", function(data) {
      console.log("webrtc_received", data.toString(), "\nfrom", this.kid);
    });
  }

  initOffer() {
    this.rtc = new simplePeer({
      initiator: true,
      config: {
        iceServers: [
          {
            urls: "stun:stun.l.google.com:19302"
          }
        ]
      },
      trickle: false,
      wrtc: wrtc
    });
  }
  initAnswer() {
    this.rtc = new simplePeer({
      initiator: false,
      config: {
        iceServers: [
          {
            urls: "stun:stun.l.google.com:19302"
          }
        ]
      },
      trickle: false,
      wrtc: wrtc
    });
  }

  connecting(_kid) {
    console.log("webrtc_connecting", _kid);
    this.kid = _kid;
    this.isConnected = false;
  }

  connected() {
    console.log("webrtc", "connected", this.kid);
    this.isConnected = true;
  }

  failed() {
    console.log("webrtc", "connectFailed", this.kid);
  }

  disconnected() {
    console.log("webrtc", "disconnected", this.kid);
    this.isConnected = false;
  }

  send(data) {
    try {
      console.log("webrtc_send", data.toString(), "\ntarget", this.kid);
      this.rtc.send(data);
      return true;
    } catch (error) {
      this.disconnected();
      return false;
    }
  }
}
