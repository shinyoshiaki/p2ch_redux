import {
  createStore as reduxCreateStore,
  applyMiddleware,
  combineReducers
} from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import connectKadReducer from "../module/Api";
import blockchainReducer from "../module/Blockchain";

export default function createStore() {
  const store = reduxCreateStore(
    combineReducers({
      api: connectKadReducer,
      blockchain: blockchainReducer
    }),
    applyMiddleware(thunk, logger)
  );
  return store;
}
