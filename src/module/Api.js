import Connect from "../api/kad/connect";
import sha1 from "sha1";
import BlockchainApp from "../api/blockchain/BlockchainApp";

const userId = sha1(Math.random().toString());

export const initialState = {
  connection: undefined,
  userId: "",
  blockchain: undefined,
  transportLayer: undefined
};

const actionType = {
  CONNECT: "CONNECT"
};

export function connectKad(dispatch) {
  const connection = new Connect(userId);
  const data = {
    connection: connection,
    blockchain: new BlockchainApp(userId, connection)
  };
  dispatch({ type: actionType.CONNECT, data: data });
  return data;
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case actionType.CONNECT:
      return {
        ...state,
        connection: action.data.connection,
        blockchain: action.data.blockchain,
        userId: action.data.connection.userId
      };
    default:
      return state;
  }
}
