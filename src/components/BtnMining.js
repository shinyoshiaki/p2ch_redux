import React, { Component } from "react";
import { Button } from "@material-ui/core";
import { mining } from "../module/Blockchain";

class BtnMining extends Component {
  mining() {
    const { dispatch, api } = this.props;
    mining(dispatch, api);
  }
  render() {
    return (
      <ul>
        <Button
          onClick={() => {
            this.mining();
          }}
        >
          mining
        </Button>
      </ul>
    );
  }
}

export default BtnMining;
