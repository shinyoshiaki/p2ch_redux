import React, { Component } from "react";
import { Button, TextField } from "@material-ui/core";
import { transaction } from "../module/Blockchain";

let tokenAmount;
let targetAddress;

class FormTransaction extends Component {
  render() {
    const { dispatch, api } = this.props;
    return (
      <div>
        <TextField
          label="target address"
          onChange={e => (targetAddress = e.target.value)}
        />
        <br />
        <TextField
          label="send amount"
          onChange={e => (tokenAmount = e.target.value)}
        />
        <br />
        <Button
          onClick={() => {
            transaction(dispatch, api, targetAddress, tokenAmount);
          }}
        >
          send
        </Button>
      </div>
    );
  }
}

export default FormTransaction;
